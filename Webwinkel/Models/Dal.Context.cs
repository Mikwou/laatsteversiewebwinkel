﻿using System.Data.Entity;

namespace Webwinkel.Models
{
    public class Dal: DbContext
    {
        public Dal()
            : base("name=WebwinkelWindowsAuthentication")
        {

        }

       public virtual DbSet<UnitBase> DbSetUnitBase { get; set; }
       public virtual DbSet<Country> DbSetCountry { get; set; }

        public class Initializer : IDatabaseInitializer<Dal>
        {
            public void InitializeDatabase(Dal context)
            {
                if (!context.Database.Exists() || !context.Database.CompatibleWithModel(false))
                {
                    if (context.Database.Exists())
                    {
                        context.Database.Delete();
                    }
                    context.Database.Create();
                }
            }
        }
    }
}